# Roomba Simulator

This project aims to create a simulation of an automated vacuum cleaner. A floor space is generated, a roomba is placed at some arbitrary point available to explore, and the goal is to maximize floor space covered in as little time as possible.