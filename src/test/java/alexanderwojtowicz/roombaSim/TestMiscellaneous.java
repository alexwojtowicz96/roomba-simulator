package alexanderwojtowicz.roombaSim;

import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import static org.junit.Assert.*;
import java.awt.Point;
import java.util.List;
import java.util.Vector;
import static org.hamcrest.CoreMatchers.*;

//=================================================================================================
public class TestMiscellaneous {
	Miscellaneous m1;    // A random Miscellaneous (Obstacle)
	Miscellaneous m2;    // A user-defined Miscellaneous (Obstacle)
	Miscellaneous m3;    // A user-defined Miscellaneous (Obstacle)
	List<Point> pList1;  // A list of pre-defined points that represent an obstacle
	List<Point> pList2;  // A list of pre-defined points that represent an obstacle
	
	@Before
	public void setup() {
		pList1 = new Vector<Point>();
		pList1.add(new Point(0, 0));
		pList1.add(new Point(0, 1));
		pList1.add(new Point(1, 2));
		pList1.add(new Point(2, 7));
		pList1.add(new Point(3, -4));
		pList1.add(new Point(5, -8));
		pList1.add(new Point(2, 3));
		pList1.add(new Point(4, 4));
		pList1.add(new Point(3, 0));
		pList1.add(new Point(-1, -1));
		pList1.add(new Point(-13, 0));
		pList1.add(new Point(-6, 0));
		pList1.add(new Point(-6, 3));
		
		pList2 = new Vector<Point>();
		pList2.add(new Point(1, 2));
		pList2.add(new Point(3, 7));
		pList2.add(new Point(4, 1));
		pList2.add(new Point(2, 1));
		pList2.add(new Point(3, 5));
		pList2.add(new Point(14, 3));
		
		m1 = new Miscellaneous(15, 10);
		m2 = new Miscellaneous(pList1);
		m3 = new Miscellaneous(pList2);
	}
//-------------------------------------------------------------------------------------------------
	@After
	public void finish() {
		printBanner(150, '*');
	}
//-------------------------------------------------------------------------------------------------
	public void printBanner(int width, char c) {
		for(int i=0; i<width; i++) {
			System.out.print(c);
		}
		System.out.print("\n");
	}
//-------------------------------------------------------------------------------------------------	
	@Test
	public void testRandomize() {
		
	}
//-------------------------------------------------------------------------------------------------	
	@Test
	public void testCreate() {
		System.out.print(m1);
	}
//-------------------------------------------------------------------------------------------------	
	@Test  // This is the user-defined way of creating miscellaneous obstacles.
	public void testConstructor1() {
		System.out.print(m2);
		System.out.print(m3);
	}
}
//=================================================================================================