package alexanderwojtowicz.roombaSim;

import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
//import java.awt.Point;

//=================================================================================================
public class TestTable {
	Table table1;  // A sample Table (Obstacle)
	Table table2;  // A sample Table (Obstacle)
	Table table3;  // A sample Table (Obstacle)
	Table table4;  // A sample Table (Obstacle)
	
	@Before
	public void setup() {
		table1 = new Table(4, 10);
		table2 = new Table(10, 4);
		table3 = new Table(5, 1);
		table4 = new Table(5, 0);
	}
//-------------------------------------------------------------------------------------------------
	@After
	public void finish() {
		printBanner(150, '*');
	}
//-------------------------------------------------------------------------------------------------
	public void printBanner(int width, char c) {
		for(int i=0; i<width; i++) {
			System.out.print(c);
		}
		System.out.print("\n");
	}
//-------------------------------------------------------------------------------------------------	
	@Test // This is simply to see everything...not exactly a test since it is tested in Obstacle.
	public void testToString() {
		System.out.println("THIS IS FOR VISUALIZATION");
		System.out.print(table1);
		System.out.print(table2);
		System.out.print(table3);
		System.out.print(table4);
	}
//-------------------------------------------------------------------------------------------------
	@Test
	public void testRandomize() {
		table1.randomize(10, 10);
		System.out.print(table1);
	}
}
//=================================================================================================