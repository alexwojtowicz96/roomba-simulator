package alexanderwojtowicz.roombaSim;

import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

//=================================================================================================
public class TestWall {
	Wall wall1;  // A sample Wall (Obstacle)
	Wall wall2;  // A sample Wall (Obstacle)
	
	@Before
	public void setup() {
		wall1 = new Wall(2, 6);
		wall2 = new Wall(20, 0);
	}
//-------------------------------------------------------------------------------------------------
	@After
	public void finish() {
		printBanner(150, '*');
	}
//-------------------------------------------------------------------------------------------------
	public void printBanner(int width, char c) {
		for(int i=0; i<width; i++) {
			System.out.print(c);
		}
		System.out.print("\n");
	}
//-------------------------------------------------------------------------------------------------	
	@Test
	public void testCreate() {
		System.out.print(wall1);
		System.out.print(wall2);
	}
//-------------------------------------------------------------------------------------------------	
	@Test
	public void testRandomize() {
		wall1.randomize(20, 10);
		wall1.transform(4, 4);
		System.out.print(wall1);
	}
}
//=================================================================================================