package alexanderwojtowicz.roombaSim;

import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

//=================================================================================================
public class TestCouch {
	Couch couch1;  // A sample Couch (Obstacle)
	Couch couch2;  // A sample Couch (Obstacle)
	Couch couch3;  // A sample Couch (Obstacle)
	Couch couch4;  // A sample Couch (Obstacle)
	Couch couch5;  // A sample Couch (Obstacle)
	Couch couch6;  // A sample Couch (Obstacle)
	
	@Before
	public void setup() {
		couch1 = new Couch(10, 8);
		couch1.setType(0);
		couch2 = new Couch(11, 8);
		couch2.setType(1);
		couch3 = new Couch(8, 5);
		couch3.setType(2);
	}
//-------------------------------------------------------------------------------------------------
	@After
	public void finish() {
		printBanner(150, '*');
	}
//-------------------------------------------------------------------------------------------------
	public void printBanner(int width, char c) {
		for(int i=0; i<width; i++) {
			System.out.print(c);
		}
		System.out.print("\n");
	}
//-------------------------------------------------------------------------------------------------	
	@Test
	public void testCreate() {
		//System.out.print(couch1);
		//System.out.print(couch2);
		//System.out.print(couch3);
	}
//-------------------------------------------------------------------------------------------------
	@Test
	public void testRandomize() {
		System.out.print(couch1);
		couch1.randomize(50, 10);
		System.out.print(couch1);
		couch1.randomize(2, 2);
		System.out.print(couch1);
	}
}
//=================================================================================================