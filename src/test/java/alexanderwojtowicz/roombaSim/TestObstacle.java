package alexanderwojtowicz.roombaSim;

import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import java.awt.Point;
import java.util.List;
import java.util.Vector;

//=================================================================================================
public class TestObstacle {
	List<Obstacle> oList;          // A list of Obstacle objects.    
	Miscellaneous randomObstacle;  // A randomly generated Miscellaneous for each test.
	Miscellaneous staticObstacle   // A Miscellaneous which remains consistent for all tests.
	= new Miscellaneous(10, 10);
	
	@Before
	public void setup() {
		oList = new Vector<Obstacle>();
		randomObstacle = new Miscellaneous(10, 10);
	}
//-------------------------------------------------------------------------------------------------
	@After
	public void finish() {
		printBanner(150, '*');
	}
//-------------------------------------------------------------------------------------------------
	public void printBanner(int width, char c) {
		for(int i=0; i<width; i++) {
			System.out.print(c);
		}
		System.out.print("\n");
	}
//-------------------------------------------------------------------------------------------------
	@Test
	public void testWidth() {
		System.out.println("WIDTH: " + staticObstacle.width());
	}
//-------------------------------------------------------------------------------------------------	
	@Test
	public void testLength() {
		System.out.println("LENGTH: " + staticObstacle.length());
	}
//-------------------------------------------------------------------------------------------------
	@Test
	public void testToString() {
		System.out.print(staticObstacle);
		System.out.println();
		staticObstacle.transform();
		System.out.print(staticObstacle);
		System.out.println();
	}
//-------------------------------------------------------------------------------------------------
	@Test
	public void testVisualize() {
		System.out.print(staticObstacle.visualize());
	}
//-------------------------------------------------------------------------------------------------
	@Test
	public void testRotate() {
		for(int i=0; i<4; i++) {
			System.out.print(staticObstacle.visualize());
			staticObstacle.rotate(1);
		}
	}
}
//=================================================================================================