package alexanderwojtowicz.roombaSim;

import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import java.io.File;
import java.io.FileNotFoundException;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

//=================================================================================================
public class TestFloorGenerator {
	FloorGenerator fGen1;  // Floor Generator 1
	FloorGenerator fGen2;  // Floor Generator 2
	Table table;           // A sample Table (Obstacle)
	Wall wall;             // A sample Wall (Obstacle)
	Couch couch;           // A sample Couch (Obstacle)
	Miscellaneous misc;    // A sample Miscellaneous (Obstacle)
	String testRes;        // **/roomba-simulator/src/test/resources/
	File log1;             // A log file: **/test/resources/log1.txt
	File log2;             // A log file: **/test/resources/log2.txt
	
	@Before
	public void setup() throws CloneNotSupportedException {
		String classLoc = TestTable.class
			.getProtectionDomain()
			.getCodeSource()
			.getLocation()
			.getPath()
			.toString();
		String resources = "../../../../src/test/resources/";
		testRes = classLoc + resources;
		log1 = new File(testRes + "log1.txt");
		log2 = new File(testRes + "log2.txt");
		
		fGen1 = new FloorGenerator();
		fGen2 = new FloorGenerator(20, 20);
		table = new Table(5, 5);
		wall  = new Wall(1, 5);
		misc  = new Miscellaneous(8, 8);
		couch = new Couch(9, 5);
		couch.setType(2);
		couch.create(9, 5);
	}
//-------------------------------------------------------------------------------------------------
	@After
	public void finish() {
		printBanner(150, '*');
	}
//-------------------------------------------------------------------------------------------------
	public void printBanner(int width, char c) {
		for(int i=0; i<width; i++) {
			System.out.print(c);
		}
		System.out.print("\n");
	}
//-------------------------------------------------------------------------------------------------
	@Test
	public void testToString() {
		//System.out.print(fGen1);
	}
//-------------------------------------------------------------------------------------------------
	@Test
	public void testGenerateObstacles() throws CloneNotSupportedException, FileNotFoundException {
		fGen2.clearObstacles();
		fGen2.generateObstacles(false, 1);
		//fGen2.addObstacle(table.clone());
		//System.out.print(fGen2);
		fGen2.log(log1);
	}
}
//=================================================================================================