package alexanderwojtowicz.roombaSim;

import java.awt.Point;
//import java.lang.Math.*;
import java.util.List;
import java.util.Vector;
import java.util.Random;

//=================================================================================================
public class Couch extends Obstacle implements Cloneable {
//-------------------------------------------------------------------------------------------------
	public List<String> TYPELIST = new Vector<String>();
//---- PRIVATE ------------------------------------------------------------------------------------
	/* 
	 * Types: 0 : Generic Rectangle
	 *        1 : L-Sectional
	 *        2 : C-Sectional
	 */
	private int type;
	
//---- CONSTRUCTORS -------------------------------------------------------------------------------
	/**
	 * Create a brand-new {@link Couch} with a specified width, length, and type.
	 * @param width
	 * @param length
	 * @param type
	 */
	public Couch(int width, int length) {
		super();	
		TYPELIST.add("Generic Rectangle");  // 0
		TYPELIST.add("L-Sectional");        // 1
		TYPELIST.add("C-Sectional");        // 2
		if(length < 2) {
			System.out.println("ERROR: Length must be > 1! Converting to '2'");
			length = 2;
		}
		if(width < 2) {
			System.out.println("ERROR: Width must be > 1! Converting to '2'");
			width = 2;
		}
		
		this.type = 0;
		this.id = 0;
		create(width, length);		
	}
	
//---- METHODS ------------------------------------------------------------------------------------
	/**
	 * Create the list of points that a couch occupies in 2D space.
	 * @param width
	 * @param length
	 * @param type
	 */
	@Override
	public void create(int width, int length) {
		this.spaceOccupied.clear();
		this.name = "Couch" + ": " + TYPELIST.get(this.type);
		if(this.type == 0) {
			for(int i=0; i<width; i++) {
				for(int j=0; j<length; j++) {
					this.spaceOccupied.add(new Point(i, j));
				}
			}
		}
		else {
			Random rand = new Random();
			int rLength = rand.nextInt(length) + 1;
			if(rLength == length) {
				rLength = length - 1;
			}
			for(int i=0; i<width; i++) {
				for(int j=0; j<rLength; j++) {
					this.spaceOccupied.add(new Point(i, j));
				}
			}
			rand = new Random();
			int rWidth = rand.nextInt(width/2) + 1;
			if(this.type == 1) {
				for(int i=0; i<rWidth; i++) {
					for(int j=length-1; j>(rLength-1); j--) {
						this.spaceOccupied.add(new Point(i, j));
					}
				}
			}
			else if(this.type == 2) {
				if(width > 3) {
					while(rWidth*2 >= width) {
						rWidth--;
					}
				}
				for(int i=0; i<rWidth; i++) {
					for(int j=length-1; j>(rLength-1); j--) {
						this.spaceOccupied.add(new Point(i, j));
					}
				}
				for(int i=(width-rWidth); i<width; i++) {
					for(int j=length-1; j>(rLength-1); j--) {
						this.spaceOccupied.add(new Point(i, j));
					}
				}
			}
		}
	}	
//-------------------------------------------------------------------------------------------------
	/**
	 * Generate a random {@link Couch} of a random type within the parameters. Overwrites
	 * existing {@link Couch}.
	 * @param boundWidth
	 * @param boundLength
	 */
	@Override
	public void randomize(int boundWidth, int boundLength) {
		Random rand = new Random();
		this.type = rand.nextInt(TYPELIST.size()) + 0;
		rand = new Random();
		int width = rand.nextInt(boundWidth);
		rand = new Random();
		int length = rand.nextInt(boundLength);
		if(width < 2) {
			width = 2;
		}
		if(length < 2) {
			length = 2;
		}
		create(width, length);
		this.name = "Couch" + ": " + TYPELIST.get(this.type);
	}
//-------------------------------------------------------------------------------------------------
	public int getTypeInt() {
		return this.type;
	}
//-------------------------------------------------------------------------------------------------
	public String getType() {
		return TYPELIST.get(this.type);
	}
//-------------------------------------------------------------------------------------------------
	public void setType(int type) {
		if(type < 0 || type > 2) {
			System.out.println("ERROR: Unknown type! Converting to '0' -> Generic Rectangle");
			type = 0;
		}
		this.type = type;
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Make a deep copy of a {@link Couch}.
	 * @throws CloneNotSupportedException
	 */
	@Override
	public Obstacle clone() throws CloneNotSupportedException {
		Couch newCouch = new Couch(this.width(), this.length());
		newCouch.id = this.id;
		newCouch.name = this.name;
		newCouch.type = this.type;
		newCouch.spaceOccupied.clear();
		for(int i=0; i<this.spaceOccupied.size(); i++) {
			newCouch.spaceOccupied.add(this.spaceOccupied.get(i));
		}
		
		return newCouch;
	}
//-------------------------------------------------------------------------------------------------
}
//=================================================================================================