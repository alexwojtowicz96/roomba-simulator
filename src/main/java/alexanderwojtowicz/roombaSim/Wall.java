package alexanderwojtowicz.roombaSim;

import java.awt.Point;
import java.util.Random;

//=================================================================================================
public class Wall extends Obstacle implements Cloneable {
//---- CONSTRUCTORS -------------------------------------------------------------------------------
	/**
	 * Create a brand-new {@link Wall}, and check for valid input.
	 * @param thickness
	 * @param length
	 */
	public Wall(int thickness, int length) {
		super();
		if(thickness < 1) {
			System.out.println("ERROR: Wall needs a thickness > 0! Converting to '1'");
			thickness = 1;
		}
		if(length < 1) {
			System.out.println("ERROR: Wall needs a length > 0! Converting to '1");
			length = 1;
		}
		
		create(thickness, length);
		this.name = "Wall";
		this.id = 0;
	}
	
//---- METHODS ------------------------------------------------------------------------------------
	/**
	 * Create the collection of points that make up the wall.
	 * @param thickness
	 * @param length
	 */
	@Override
	public void create(int thickness, int length) {
		this.spaceOccupied.clear();
		for(int i=0; i<thickness; i++) {
			for(int j=0; j<length; j++) {
				this.spaceOccupied.add(new Point(i, j));
			}
		}
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Randomize the parameters of the {@link Wall}. This overrides the existing {@link Wall}.
	 * @param boundThickness
	 * @param boundLength
	 */
	@Override
	public void randomize(int boundThickness, int boundLength) {
		Random rand = new Random();
		int thickness = rand.nextInt(boundThickness) + 1;
		rand = new Random();
		int length = rand.nextInt(boundLength) + 1;
		create(thickness, length);
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Make a deep copy of a {@link Wall}.
	 * @throws CloneNotSupportedException
	 */
	@Override
	public Obstacle clone() throws CloneNotSupportedException {
		Wall newWall = new Wall(this.width(), this.length());

		return newWall;
	}
//-------------------------------------------------------------------------------------------------
}
//=================================================================================================