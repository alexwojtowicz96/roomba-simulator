package alexanderwojtowicz.roombaSim;

import java.awt.Point;
//import java.lang.Math.*;
import java.util.List;
import java.util.Vector;
import java.util.Random;

/**
 * A {@link Miscellaneous} is an {@link Obstacle} with a random assortment of 
 * spaces being occupied. These may be user-defined or randomly decided.
 */
//=================================================================================================
public class Miscellaneous extends Obstacle implements Cloneable {
//---- CONSTRUCTORS -------------------------------------------------------------------------------
	/**
	 * Create a {@link Miscellaneous} {@link Obstacle} with a user-defined set of points.
	 * @param spaceOccupied
	 */
	public Miscellaneous(List<Point> spaceOccupied) {
		super(spaceOccupied);
		this.id = 0;
	}
	
	/**
	 * Create a {@link Miscellaneous} {@link Obstacle} (at random) within the parameters.
	 * @param width
	 * @param length
	 */
	public Miscellaneous(int width, int length) {
		super();
		if(width < 1) {
			System.out.println("ERROR: width is < 1! Converting... width = 1");
			width = 1;
		}
		if(length < 1) {
			System.out.println("ERROR: length is < 1! Converting... length = 1");
			length = 1;
		}
		
		this.id = 0;
		this.name = "Random Miscellaneous";
		create(width, length);
	}	
//---- METHODS ------------------------------------------------------------------------------------
	/** 
	 * Creates a random {@link Miscellaneous} within the supplied parameters.
	 * @param width
	 * @param length
	 */
	@Override
	public void create(int width, int length) {
		this.spaceOccupied.clear();
		Random rand = new Random();
		for(int i=0; i<width; i++) {
			for(int j=0; j<length; j++) {
				int willExist = rand.nextInt(2);
				if(willExist == 1) {
					this.spaceOccupied.add(new Point(i, j));
				}
			}
		}
		this.name = "Random Miscellaneous";
	}
//-------------------------------------------------------------------------------------------------
	/** TODO
	 * Creates random bounds within the parameters and overwrites 
	 * the existing {@link Miscellaneous} {@link Obstacle}.
	 * @param boundWidth
	 * @param boundLength
	 */
	@Override
	public void randomize(int boundWidth, int boundLength) {
		Random rand = new Random();
		int width = rand.nextInt(boundWidth) + 1;
		rand = new Random();
		int length = rand.nextInt(boundLength) + 1;
		create(width, length);
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Make a deep copy of a {@link Miscellaneous}.
	 * @throws CloneNotSupportedException
	 */
	@Override
	public Obstacle clone() throws CloneNotSupportedException {
		Miscellaneous newMisc = new Miscellaneous(this.spaceOccupied);
		newMisc.id = this.id;
		newMisc.name = this.name;
		
		return newMisc;
	}
//-------------------------------------------------------------------------------------------------
}
//=================================================================================================