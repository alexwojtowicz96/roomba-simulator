package alexanderwojtowicz.roombaSim;

import java.awt.Point;
//import java.lang.Math.*;
import java.util.List;
import java.util.Vector;

/**
 * Obstacles are furniture, pets, stairs, walls, etc. 
 */
//=================================================================================================
public abstract class Obstacle implements Cloneable {
//---- PRIVATE ------------------------------------------------------------------------------------
	protected List<Point> spaceOccupied;
	protected String name;
	protected int id;
	
//---- CONSTRUCTORS -------------------------------------------------------------------------------
	/**
	 * Create an {@link Obstacle}.
	 */
	public Obstacle() {
		this.spaceOccupied = new Vector<Point>();
		Point p = new Point(0, 0);
		spaceOccupied.add(p);
		name = "Generic Obstacle";
		id = 0;
	}	
	
	/**
	 * Create an {@link Obstacle} with a pre-defined list of points being occupied.
	 */
	public Obstacle(List<Point> spaceOccupied) {
		this.spaceOccupied = new Vector<Point>();
		for(int i=0; i<spaceOccupied.size(); i++) {
			this.spaceOccupied.add(spaceOccupied.get(i));
		}
		name = "User-Defined Obstacle";
	}
	
//---- METHODS ------------------------------------------------------------------------------------
	/**
	 * Print a point at some index in a List<Point>. (Hacked Point.toString() override).
	 * @param index
	 * @param pList
	 * @return string
	 */
	public String getPoint(int index, List<Point> pList) {
		String string = new String();
		string = string 
				+ "(" 
				+ pList.get(index).x
				+ ", "
				+ pList.get(index).y
				+ ")";
		
		return string;
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Print the visualization of the object to the screen.
	 * @return string
	 */
	public String visualize() {
		String string = new String();
		List<String> y = new Vector<String>();
		List<String> x = new Vector<String>();
		int maxYLength = 0;
		int maxXLength = 0;
		
		string = string + "\n";
		for(Integer i=max('x'); i>min('x')-1; i--) {
			String currNum = i.toString();
			if(currNum.length() > maxXLength) {
				maxXLength = currNum.length();
			}
			x.add(currNum);
		}
		for(Integer i=max('y'); i>min('y')-1; i--) {
			String currNum = i.toString();
			if(currNum.length() > maxYLength) {
				maxYLength = currNum.length();
			}
			y.add(currNum);
		}
		for(int j=0; j<maxYLength+1; j++) {
			string = string + " ";
		}
		for(int i=0; i<x.size(); i++) {
			string = string + "_";
			if(i != x.size()-1) {
				for(int j=0; j<maxXLength; j++) {
					string = string + "_";
				}
			}
		}
		string = string + "_\n";
		for(int i=0; i<y.size(); i++) {
			String yBuffer = " ";
			for(int j=0; j<(maxYLength-y.get(i).length()); j++) {
				yBuffer = yBuffer + " ";
			}
			string = string + yBuffer + y.get(i) + "|";
			for(int j=0; j<x.size(); j++) {
				int xVal = Integer.parseInt(x.get((x.size()-1)-j));
				int yVal = Integer.parseInt(y.get(i));
				Point toCompare = new Point(xVal, yVal);
				boolean exists = false;
				for(int k=0; k<this.spaceOccupied.size(); k++) {
					if(spaceOccupied.get(k).equals(toCompare)) {
						exists = true;
						break;
					}
				}
				if(!y.get(i).equals("0")) {
					if(x.get((x.size()-1)-j).equals("0")) {
						if(!exists) {
							string = string + "|";
						}
						else {
							string = string + "X";
						}
					}
					else {
						if(!exists) {
							string = string + ".";
						}
						else {
							string = string + "X";
						}
					}
					for(int k=0; k<maxXLength; k++) {
						string = string + " ";
					}
				}
				else {
					if(x.get((x.size()-1)-j).equals("0")) {
						if(!exists) {
							string = string + "+";
						}
						else {
							string = string + "X";
						}
					}
					else {
						if(!exists) {
							string = string + "-";	
						}
						else {
							string = string + "X";
						}
					}
					if(j != x.size()-1) {
						for(int k=0; k<maxXLength; k++) {
							string = string + "-";
						}
					}
				}
			}
			string = string + "\n";
		}
		for(int j=0; j<maxYLength+1; j++) {
			string = string + " ";
		}
		string = string + ">";
		for(int i=0; i<x.size(); i++) {
			string = string + "+";
			if(i != x.size()-1) {
				for(int j=0; j<maxXLength; j++) {
					string = string + "-";
				}
			}
		}
		string = string + "\n";
		for(int i=0; i<maxYLength+2; i++) {
			string = string + " ";
		}
		for(int i=0; i<x.size(); i++) {
			string = string + "\\";
			for(int k=0; k<maxXLength; k++) {
				string = string + " ";
			}
		}
		string = string + "\n";
		for(int i=0; i<maxYLength+2; i++) {
			string = string + " ";
		}
		for(int i=x.size()-1; i>-1; i--) {
			string = string + x.get(i);
			for(int k=0; k<(maxXLength-x.get(i).length())+1; k++) {
				string = string + " ";
			}
		}
		string = string + "\n";
		
		return string;
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * This merely exists to visualize the object in 2D space.
	 */
	@Override
	public String toString() {
		String string = new String();
		String buffer = "              ";
		String div = "==================================================";
		string = string + div + "\n";
		string = string + "NAME     :    " + this.name + "\n";
		string = string + "ID       :    " + this.id + "\n";
		string = string + "WIDTH    :    " + width() + "\n";
		string = string + "LENGTH   :    " + length() + "\n\n";
		string = string + "POINTS   :    ";
		if(!spaceOccupied.equals(new Vector<Point>())) {
			for(int i=0; i<spaceOccupied.size(); i++) {
				if(i == 0) {
					string = string + getPoint(i, spaceOccupied);
				}
				else {
					string = string + buffer + getPoint(i, spaceOccupied);
				}
				string = string + "\n";
			}
		}
		else {
			string = string + "N/A" + "\n";
		}
		string = string + "\n";
		string = string + "BOUNDBOX :    ";
		if(!spaceOccupied.equals(new Vector<Point>())) {
			for(int i=0; i<boundingBox().size(); i++) {
				if(i == 0) {
					string = string + getPoint(i, boundingBox());
				}
				else {
					string = string + buffer + getPoint(i, boundingBox());
				}
				string = string + "\n";
			}
		}
		else {
			string = string + "N/A" + "\n";
		}
		string = string + "\n" + "VISUALIZATION"; 
		string = string + visualize();
		string = string + div + "\n";
		
		return string;
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Get the width of the obstacle.
	 * @return width
	 */
	public int width() {
		int lowX = Integer.MAX_VALUE;
		int highX = Integer.MIN_VALUE;
		for(int i=0; i<spaceOccupied.size(); i++) {
			if(spaceOccupied.get(i).x < lowX) {
				lowX = spaceOccupied.get(i).x;
			}
			if(spaceOccupied.get(i).x > highX) {
				highX = spaceOccupied.get(i).x;
			}
		}
		
		return (highX - lowX) + 1;
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Get the length of the obstacle.
	 * @return length
	 */
	public int length() {
		int lowY = Integer.MAX_VALUE;
		int highY = Integer.MIN_VALUE;
		for(int i=0; i<spaceOccupied.size(); i++) {
			if(spaceOccupied.get(i).y < lowY) {
				lowY = spaceOccupied.get(i).y;
			}
			if(spaceOccupied.get(i).y > highY) {
				highY = spaceOccupied.get(i).y;
			}
		}
		
		return (highY - lowY) + 1;
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Rotate the obstacle 90 degrees counterclockwise about the origin.
	 */
	public void rotate() {
		for(int i=0; i<this.spaceOccupied.size(); i++) {
			int x = this.spaceOccupied.get(i).x;
			int y = this.spaceOccupied.get(i).y;
			this.spaceOccupied.get(i).x = (0 - y);
			this.spaceOccupied.get(i).y = x;
		}
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Call the Obstacle::rotate() method n number of times.
	 * @param x
	 */
	public void rotate(int n) {
		for(int i=0; i<n; i++) {
			rotate();
		}
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Make every x and y component in the list of points being occupied >= 0.
	 * Also, something must be touching the y-axis, and something must be touching the x-axis.
	 */
	public void transform() {
		int xToAdd = Integer.MAX_VALUE;
		int yToAdd = Integer.MAX_VALUE;
		for(int i=0; i<spaceOccupied.size(); i++) {
			if(spaceOccupied.get(i).x < xToAdd) {
				xToAdd = spaceOccupied.get(i).x;
			}
			if(spaceOccupied.get(i).y < yToAdd) {
				yToAdd = spaceOccupied.get(i).y;
			}
		}
		if(xToAdd >= 0) {
			xToAdd = 0;
		}
		else {
			xToAdd = Math.abs(xToAdd);
		}
		if(yToAdd >= 0) {
			yToAdd = 0;
		}
		else {
			yToAdd = Math.abs(yToAdd);
		}
		for(int i=0; i<spaceOccupied.size(); i++) {
			spaceOccupied.get(i).x += xToAdd;
			spaceOccupied.get(i).y += yToAdd;
		}

		xToAdd = Integer.MAX_VALUE;
		yToAdd = Integer.MAX_VALUE;
		for(int i=0; i<spaceOccupied.size(); i++) {
			if(spaceOccupied.get(i).x < xToAdd) {
				xToAdd = spaceOccupied.get(i).x;
			}
			if(spaceOccupied.get(i).y < yToAdd) {
				yToAdd = spaceOccupied.get(i).y;
			}
		}
		for(int i=0; i<spaceOccupied.size(); i++) {
			spaceOccupied.get(i).x -= xToAdd;
			spaceOccupied.get(i).y -= yToAdd;
		}
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Transform the {@link Obstacle} according to given x and y values.
	 * @param x
	 * @param y
	 */
	public void transform(int x, int y) {
		for(int i=0; i<this.spaceOccupied.size(); i++) {
			this.spaceOccupied.get(i).x += x;
			this.spaceOccupied.get(i).y += y;
		}
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Depending on if param is x or y, find the highest x or y value in spaceOccupied.
	 * @param var 'x' or 'y'
	 * @return max
	 */
	public int max(char var) {
		int max = Integer.MIN_VALUE;
		if(var == 'x') {
			for(int i=0; i<spaceOccupied.size(); i++) {
				if(spaceOccupied.get(i).x > max) {
					max = spaceOccupied.get(i).x;
				}
			}
		}
		else if(var == 'y') {
			for(int i=0; i<spaceOccupied.size(); i++) {
				if(spaceOccupied.get(i).y > max) {
					max = spaceOccupied.get(i).y;
				}
			}
		}
		else {
			System.out.println("ERROR: Obstacle::max(). '" + var + "' is not 'x' or 'y'!");
		}
		
		return max;
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Depending on if param is x or y, find the lowest x or y value in spaceOccupied.
	 * @param var 'x' or 'y'
	 * @return min
	 */
	public int min(char var) {
		int min = Integer.MAX_VALUE;
		if(var == 'x') {
			for(int i=0; i<spaceOccupied.size(); i++) {
				if(spaceOccupied.get(i).x < min) {
					min = spaceOccupied.get(i).x;
				}
			}
		}
		else if(var == 'y') {
			for(int i=0; i<spaceOccupied.size(); i++) {
				if(spaceOccupied.get(i).y < min) {
					min = spaceOccupied.get(i).y;
				}
			}
		}
		else {
			System.out.println("ERROR: Obstacle::min(). '" + var + "' is not 'x' or 'y'!");
		}
		
		return min;
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Grab the four corners of the imaginary box that surrounds the {@link Obstacle}.
	 * @return corners
	 */
	public List<Point> boundingBox() {
		List<Point> corners = new Vector<Point>();
		char x = 'x';
		char y = 'y';
		corners.add(new Point(min(x), min(y)));
		corners.add(new Point(max(x), min(y)));
		corners.add(new Point(min(x), max(y)));
		corners.add(new Point(max(x), max(y)));
		
		return corners;
	}
//-------------------------------------------------------------------------------------------------
	public String getName() {
		return name;
	}
	
	public int getId() {
		return id;
	}
//-------------------------------------------------------------------------------------------------
	public abstract Obstacle clone() throws CloneNotSupportedException; 
	public abstract void randomize(int width, int length);
	public abstract void create(int width, int length);
}
//=================================================================================================