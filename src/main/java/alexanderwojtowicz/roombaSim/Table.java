package alexanderwojtowicz.roombaSim;

import java.awt.Point;
//import java.lang.Math.*;
//import java.util.List;
//import java.util.Vector;
import java.util.Random;

//=================================================================================================
public class Table extends Obstacle implements Cloneable {	
//---- CONSTRUCTORS -------------------------------------------------------------------------------
	/**
	 * Create a brand-new {@link Table} with a width and length.
	 * @param width
	 * @param length
	 */
	public Table(int width, int length) {
		super();
		if(length < 1) {
			System.out.println("ERROR: Length must be positive! Converting to '1'");
			length = 1;
		}
		if(width < 1) {
			System.out.println("ERROR: Width must be positive! Converting to '1'");
			width = 1;
		}
		create(width, length);
		this.id = 0;
		this.name = "Table";
	}
	
//---- METHODS ------------------------------------------------------------------------------------
	/**
	 * Overwrites the existing table with a random within the bounds.
	 * @param boundWidth
	 * @param boundLength
	 */
	@Override
	public void randomize(int boundWidth, int boundLength) {
		Random rand = new Random();
		int width = rand.nextInt(boundWidth) + 1;
		rand = new Random();
		int length = rand.nextInt(boundLength) + 1;
		create(width, length);
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Create a table with a width, length and tall option.
	 * @param width
	 * @param length
	 * @param isTall
	 */
	@Override
	public void create(int width, int length) {
		this.spaceOccupied.clear();
		this.spaceOccupied.add(new Point(0, 0));
		this.spaceOccupied.add(new Point(width-1, 0));
		this.spaceOccupied.add(new Point(0, length-1));
		this.spaceOccupied.add(new Point(width-1, length-1));
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Make a deep copy of a {@link Table}.
	 * @throws CloneNotSupportedException
	 */
	@Override
	public Obstacle clone() throws CloneNotSupportedException {
		Table newTable = new Table(this.width(), this.length());
		newTable.id = this.id;
		newTable.name = this.name;
		
		return newTable;
	}	
//-------------------------------------------------------------------------------------------------
}
//=================================================================================================