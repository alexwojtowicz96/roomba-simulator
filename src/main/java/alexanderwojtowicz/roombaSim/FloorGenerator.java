package alexanderwojtowicz.roombaSim;

import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.Date;

//=================================================================================================
public class FloorGenerator {
//-------------------------------------------------------------------------------------------------
	static public final int NUM_OBSTACLES = 4;
	static public int MIN_WIDTH  = 15;  
	static public int MAX_WIDTH  = 100;
	static public int MIN_LENGTH = 15;
	static public int MAX_LENGTH = 100;
	
//----- PRIVATE -----------------------------------------------------------------------------------
	private class FloorGrid {
		final char aChar = '.';
		final char uChar = '@';
		int length;
		int width;
		Vector<Vector<Character>> grid;
		List<Point> unavailable;
		List<Point> available;
		
		public FloorGrid(int length, int width) {
			this.length = length;
			this.width = width;
			unavailable = new Vector<Point>();
			available = new Vector<Point>();
			for(int i=0; i<this.width; i++) {
				for(int j=0; j<this.length; j++) {
					available.add(new Point(i, j));
				}
			}
			populate(length, width);
		}
		
		public void populate(int length, int width) {
			this.grid = new Vector<Vector<Character>>();
			for(int i=0; i<length; i++) {
				Vector<Character> currRow = new Vector<Character>();
				for(int j=0; j<width; j++) {
					if(unavailable.contains(new Point(j, i))) {
						currRow.add(uChar);
					}
					else {
						currRow.add(aChar);
					}
				}
				grid.add(currRow);
			}
			this.unavailable = rmDups(this.unavailable);
			this.available = rmDups(this.available);
		}
		
		public void setUnavailable(int x, int y) {
			this.unavailable.add(new Point(x, y));
			populate(this.length, this.width);
			if(this.available.contains(new Point(x, y))) {
				this.available.remove(new Point(x, y));
			}
		}
		
		public List<Point> rmDups(List<Point> pList) {
			List<Point> updated = new Vector<Point>();
			for(int i=0; i<pList.size(); i++) {
				if(!updated.contains(pList.get(i))) {
					updated.add(pList.get(i));
				}
			}
			
			return updated;
		}
		
		@Override
		public String toString() {
			String string = new String();
			String indent = "  ";
			String nl = "\n";
			string = string + "FLOORGRID -> " + (width*length) + " spaces" + nl;
			string = string + indent + "Unavailable: " + this.unavailable.size() + " points" + nl;
			for(int i=0; i<this.unavailable.size(); i++) {
				string = string + indent + indent + getPoint(this.unavailable, i) + nl;
			}
			string = string + nl;
			string = string + indent + "Available: " + this.available.size() + " points" + nl;
			for(int i=0; i<this.available.size(); i++) {
				string = string + indent + indent + getPoint(this.available, i) + nl;
			}
			string = string + nl;
			
			return string;
		}
		
		public String getPoint(List<Point> pList, int index) {
			String string = new String();
			string = string + "(" 
					+ pList.get(index).x
					+ ", "
					+ pList.get(index).y
					+ ")";
			
			return string;
		}
	} // End of FloorGrid
	
	private FloorGrid floorGrid;
	private List<Obstacle> obstacles;
	
//----- CONSTRUCTORS ------------------------------------------------------------------------------
	/**
	 * Randomly generate a floor grid.
	 * @throws CloneNotSupportedException 
	 */
	public FloorGenerator() throws CloneNotSupportedException {
		Random rand = new Random();
		int length = rand.nextInt((MAX_LENGTH-MIN_LENGTH)+1) + MIN_LENGTH;
		int width = rand.nextInt((MAX_WIDTH-MIN_WIDTH)+1) + MIN_WIDTH;
		floorGrid = new FloorGrid(length, width);
		obstacles = new Vector<Obstacle>();
		generateObstacles(true, 10);
	}
	
	/**
	 * Generate a floor grid with pre-defined size.
	 * @throws CloneNotSupportedException 
	 */
	public FloorGenerator(int length, int width) throws CloneNotSupportedException {
		floorGrid = new FloorGrid(length, width);
		obstacles = new Vector<Obstacle>();
		generateObstacles(true, 10);
	}	
//----- METHODS -----------------------------------------------------------------------------------
	/**
	 * Clear this.obstacles.
	 */
	public void clearObstacles() {
		this.obstacles.clear();
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Add an {@link Obstacle} to the end of this.obstacles.
	 * @param obstacle
	 */
	public void addObstacle(Obstacle obstacle) {
		this.obstacles.add(obstacle);
	}
//-------------------------------------------------------------------------------------------------
	@Override
	/**
	 * A string version of the floor (generator).
	 */
	public String toString() {
		String string = new String();
		string = string + "FLOOR WIDTH     :     " + floorGrid.width + "\n";
		string = string + "FLOOR LENGTH    :     " + floorGrid.length + "\n";
		string = string + "OBSTACLES       :     ";
		string = string + "Total -> " + obstacles.size() + "\n";
		String buffer1 = "..................... ";
		String buffer2 = "                      ";
		if(this.obstacles.isEmpty()) {
			string = string + "N/A" + "\n";
		}
		else {
			for(int i=0; i<obstacles.size(); i++) {
				string = string + buffer1 + obstacles.get(i).getName() + "\n";
				string = string + buffer2 + "  Width  : " + obstacles.get(i).width() + "\n";
				string = string + buffer2 + "  Length : " + obstacles.get(i).length() + "\n\n";
			}
		}
		String horizontal = new String();
		for(int i=0; i<(((this.floorGrid.width+2)*2)-3); i++) {
			horizontal = horizontal + "-";
		}
		horizontal = horizontal + "\n";
		string = string + horizontal;
		for(int i=this.floorGrid.length-1; i>-1; i--) {
			String row = "|";
			for(int j=0; j<floorGrid.width; j++) {
				row = row + this.floorGrid.grid.get(i).get(j);
				if(j != this.floorGrid.width-1) {
					row = row + " ";
				}
				else {
					row = row + "|";
				}
			}
			string = string + row;
			string = string + "\n";
		}
		string = string + horizontal + "\n";
		string = string + this.floorGrid.toString();
		
		return string;
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Generate a random list of {@link Obstacle}s of a randomized size.
	 * @param random If we want a random number of obstacles, set this to true.
	 * @param num If random is false, use this as the number of obstacles to generate.
	 * @throws CloneNotSupportedException 
	 */
	public void generateObstacles(boolean random, int num) throws CloneNotSupportedException {
		final int creationFactor = 3;
		Random rand = new Random();
		int max = floorGrid.length;
		int min = floorGrid.length;
		if(floorGrid.width > floorGrid.length) {
			max = floorGrid.width;
		}
		if(floorGrid.width < floorGrid.length) {
			min = floorGrid.width;
		}
		int numToCreate = num;
		if(random) {
		numToCreate = rand.nextInt(
				(floorGrid.width*floorGrid.length)/(max*creationFactor)) + 1;
		}
		//System.out.println("numToCreate: " + numToCreate);
		for(int i=0; i<numToCreate; i++) {
			int picker = rand.nextInt(NUM_OBSTACLES) + 1;
			//System.out.println("picker: " + picker);
			if(picker == 1) {
				int bound1 = rand.nextInt(min/2) + 1;
				int bound2 = rand.nextInt(min/2) + 1;
				Table table = new Table(bound1, bound2);
				if(canPlace(table)) {
					obstacles.add(new Table(bound1, bound2));
					place(table);
				}
			}
			else if(picker == 2) {
				int bound1 = rand.nextInt(min/2) + 2;
				int bound2 = rand.nextInt(min/4) + 2;
				Couch couch = new Couch(bound1, bound2);
				couch.randomize(bound1, bound2);
				if(canPlace(couch)) {
					obstacles.add(couch);
					place(couch);
				}
			}
			else if(picker == 3) {
				int bound1 = rand.nextInt(min/2) + 1;
				int bound2 = rand.nextInt(min/4) + 1;
				Wall wall = new Wall(bound1, bound2);
				if(canPlace(wall)) {
					obstacles.add(new Wall(bound1, bound2));
					place(wall);
				}
			}
			else if(picker == 4) {
				int bound1 = rand.nextInt(min/2) + 1;
				int bound2 = rand.nextInt(min/2) + 1;
				Miscellaneous misc = new Miscellaneous(bound1, bound2);
				if(canPlace(misc)) {
					obstacles.add(new Miscellaneous(bound1, bound2));
					place(misc);
				}
			}
			else {
				System.err.println("ERROR: Could not create obstacle. Obstacle does not exist!");
			}	
		}
	}
//-------------------------------------------------------------------------------------------------
	/** TODO
	 * Place an object in a random location on the floorGrid.
	 * @param obstacle
	 * @throws CloneNotSupportedException 
	 */
	public void place(Obstacle obstacle) throws CloneNotSupportedException {
		/*Obstacle obst = new Couch(5, 10);
		obst.randomize(8, 16);
		//obst.rotate();
		obst.transform();
		//obst.transform(4, 9);
		*/
		obstacle = obstacles.get(0).clone();
		//obstacle.transform();
		//obstacle.transform(5, 5);
		for(int i=0; i<obstacle.spaceOccupied.size(); i++) {
			this.floorGrid.setUnavailable(obstacle.spaceOccupied.get(i).x,
					obstacle.spaceOccupied.get(i).y);
		}
	}
//-------------------------------------------------------------------------------------------------
	/** TODO
	 * Return a list of bounding boxes which are available to place an obstacle of a
	 * given size.
	 */
	public List<Vector<Point>> availableBoxes(List<Point> obstBox, int width, int length) {
		List<Vector<Point>> availBoxes = new Vector<Vector<Point>>();
		
		return availBoxes;
	}
//-------------------------------------------------------------------------------------------------
	/** TODO
	 * Return true if there is enough floor space to add the {@link Obstacle}.
	 * If we find at least one bounding box available, then we can place the obstacle.
	 */
	public boolean canPlace(Obstacle obstacle) {
		/*return (availableBoxes(obstacle.boundingBox(), 
				obstacle.width(), 
				obstacle.length()).size() > 0);
		*/
		return true;
	}
//-------------------------------------------------------------------------------------------------
	/**
	 * Prints the {@link FloorGenerator} to a log.
	 * @param filePath
	 * @throws FileNotFoundException 
	 */
	public void log(File filePath) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(filePath);
		System.out.println("Writing log to file: " + filePath.toString());
		Date date = new Date();
		int hSize = date.toString().length() + 4;
		String hLine = new String();
		for(int i=0; i<hSize; i++) {
			if((i == 0) || (i == (hSize-1))) {
				hLine = hLine + "+";
			}
			else {
				hLine = hLine + "-";
			}
		}
		String string = new String();
		string = string + hLine + "\n";
		String title = "| FLOOR_GENERATOR LOG";
		string = string + title;
		for(int i=0; i<(hSize-title.length()); i++) {
			if(i == ((hSize)-(title.length())-1)) {
				string = string + "|";
			}
			else {
				string = string + " ";
			}
		}
		string = string + "\n";
		string = string + "| " + date.toString() + " |" + "\n";
		string = string + hLine + "\n\n";
		string = string + this.toString();
		string = string + "OBSTACLE SUMMARY" + "\n";
		for(int i=0; i<this.obstacles.size(); i++) {
			string = string + obstacles.get(i).toString();
		}
		string = string + "\n" + "*** End of Log";
		writer.print(string);
		writer.close();
		System.out.println("Successfully saved log at: " + filePath.toString());
	}
//-------------------------------------------------------------------------------------------------
}
//=================================================================================================